#!/bin/bash


if [ $# -lt 1 ]; then
  echo "Usage: $0 offline|online" && exit
fi

if [[ ! $1 =~ ^(online|offline)$ ]]; then 
  echo "Usage: $0 offline|online" && exit
fi


mode=$1

# Switch NetEye repos
for i in CentOS-NetEye.repo CentOS-NetEye-extras.repo CentOS-BaseWP.repo
do
if [ -L /etc/yum.repos.d/$i ]
then
	echo "[+] removing symbolink link /etc/yum.repos.d/$i" 
	rm -f /etc/yum.repos.d/$i
elif [ -f /etc/yum.repos.d/$i ] 
then
	echo "[+] created backup of regular file /etc/yum.repos.d/$i" 
	mv /etc/yum.repos.d/$i /etc/yum.repos.d/$i.backup
fi
	echo "[+] Switching $i to $mode mode"
	if [ -f /etc/yum.repos.d/$i.$mode ]
	then
	    ln -s /etc/yum.repos.d/$i.$mode /etc/yum.repos.d/$i
	else
	    echo "[+] /etc/yum.repos.d/$i.$mode not found. skipping."
	fi
done

echo "[+] Cleaning yum"
yum clean all --enablerepo=neteye*
