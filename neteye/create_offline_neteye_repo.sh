EXPORT_PATH="/offline_repos/export"

if [ ! -z $1 ]; then
	EXPORT_PATH=$1
fi

if [ ! -d $EXPORT_PATH ]; then
	echo "[-] $EXPORT_PATH is not a directory or does not exist"
	exit 1
fi

if [ ! -z "$(ls -A $EXPORT_PATH 2>/dev/null )" ]; then
	echo "[-] $EXPORT_PATH is not empty. Local repos will be updated."

fi

echo "[+] Creating CentOS repos ... "

#update centos repos
for i in base os  extras updates; do cd $EXPORT_PATH; mkdir -p $EXPORT_PATH/$i; repoquery -a --repoid=$i |  xargs repotrack --repoid=$i -a x86_64 -p $EXPORT_PATH/$i/; createrepo $i;   done 
#
## Repeat for each NetEye Release
YUM0=4.15
echo "[+] Creating $YUM0 repos ... "
for i in neteye neteye-extras neteye-elastic neteye-epel neteye-ntop neteye-os   neteye-perl neteye-scl ; do cd $EXPORT_PATH; mkdir -p $EXPORT_PATH/$i; repoquery -a --repoid=$i |  xargs repotrack --repoid=$i -a x86_64 -p $EXPORT_PATH/$i/;  createrepo $i;   echo $i | grep '-' > /dev/null &&  mv $i neteye-$YUM0-`echo $i| cut -d- -f2` || mv $i $i-$YUM0;  done 
#
YUM0=4.16
echo "[+] Creating $YUM0 repos ... "
for i in neteye neteye-extras neteye-elastic neteye-epel neteye-ntop neteye-os   neteye-perl neteye-scl ; do cd $EXPORT_PATH; mkdir -p $EXPORT_PATH/$i; repoquery -a --repoid=$i |  xargs repotrack --repoid=$i -a x86_64 -p $EXPORT_PATH/$i/;  createrepo $i;   echo $i | grep '-' > /dev/null &&  mv $i neteye-$YUM0-`echo $i| cut -d- -f2` || mv $i $i-$YUM0;  done 

for i in base extras neteye-4.15 neteye-4.15-elastic neteye-4.15-epel neteye-4.15-extras neteye-4.15-ntop neteye-4.15-os neteye-4.15-perl neteye-4.15-scl neteye-4.16 neteye-4.16-elastic neteye-4.16-epel neteye-4.16-extras neteye-4.16-ntop neteye-4.16-os neteye-4.16-perl neteye-4.16-scl os updates;
 do
 wget --quiet --recursive --no-parent -R "index.html*" -A "*comps.xml"   https://repo.wuerth-phoenix.com/centos7-x86_64/RPMS.$i/repodata/ > /dev/null 
 mv repo.wuerth-phoenix.com/centos7-x86_64/RPMS.$i/repodata/*-comps.xml $EXPORT_PATH/$i/repodata/comps.xml 
done

