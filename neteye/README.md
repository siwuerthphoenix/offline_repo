NetEye4 offline repo
====================

This guide helps to perform an offline update and upgrade of a neteye system. Depending on the repositories versions the content and procedure may vary.

Included are the following repositories:

* base
* extras
* updates
* neteye-4.15
* neteye-4.15-elastic
* neteye-4.15-epel
* neteye-4.15-ntop
* neteye-4.15-os
* neteye-4.15-perl
* neteye-4.15-scl
* neteye-4.16
* neteye-4.16-elastic
* neteye-4.16-epel
* neteye-4.16-extras
* neteye-4.16-ntop
* neteye-4.16-os
* neteye-4.16-perl
* neteye-4.16-scl

## File list
```
export/        		<- directory containing the actual repos
README.md		<- this README file
make_repos_offline.sh   <- script to adapt yum configuration 
switch_repos.sh		<- script to switch from offline to online 
```


## Instructions

1. Make sure to have enough disk space (>20GB) on root `/` partition
2. Create destination dir `mkdir /offline_repos/`
3. Extract contents of archive to `/offline_repos/` : `tar xzfv neteye_offline.tar.gz /offline_repos/`
4. Navigate to directory `cd /offline_repo/` 
5. Adapt YUM configuration: `./make_repos_offline.sh`
6. Switch to offline mode: `./switch_repos.sh offline`
7. Now proceed with normal update 


To roll back to online mode, use the same script: `./switch_repos.sh online`

