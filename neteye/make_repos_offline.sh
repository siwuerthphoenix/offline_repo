#!/bin/bash

OFFLINE_REPO_PATH="\/offline_repos\/export\/"

# Put -i in the following var to do actual subs
DRY_RUN="-i"

# backup actual config
cp -r /etc/yum.repos.d/ /etc/yum.repos.d`date +%Y%m%d%H%M%S`/ 

# Switch NetEye repos
for i in CentOS-NetEye.repo CentOS-NetEye-extras.repo CentOS-BaseWP.repo
do
	cp -f /etc/yum.repos.d/$i /etc/yum.repos.d/${i}.online
	sed $DRY_RUN "s/^baseurl=.*\/centos7-x86_64\/RPMS\.\(.*\)/baseurl=file:\/\/$OFFLINE_REPO_PATH\1/g" /etc/yum.repos.d/$i
	sed $DRY_RUN 's/gpgcheck=1/gpgcheck=0/g'  /etc/yum.repos.d/$i
	mv /etc/yum.repos.d/$i /etc/yum.repos.d/$i.offline
done



